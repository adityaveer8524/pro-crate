from django.contrib import admin
from .models import Profile
from django.conf import settings


# Register your models here.
admin.site.register(Profile)
