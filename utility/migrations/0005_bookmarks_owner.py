# Generated by Django 2.0.7 on 2019-04-18 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utility', '0004_comments_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmarks',
            name='owner',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
