from django.contrib import admin
from .models import bookmarks, comments
# Register your models here
admin.site.register(bookmarks)
admin.site.register(comments)
