ABSTRACT
Procrate is an attractive, easy, convenient and thoughtfully created project sharing platform, build for students, faculty or enthusiast, where they can upload and share the projects and ideas conveniently.
Procrate is sharing projects and ideas, asking questions/doubts, searching for projects, reading and writing blogs, following other users and much more. Procrate is equipped with HASHTAG feature which optimizes the Recommendation and Search functions to meet users� requirements as per his/her profile or preference. User�s rank is decided on XP i.e. Reward Points on access of multiple features of the Procrate the User will keep on earning the Reward Point, providing User a motivation to explore the Procrate frequently.
An algorithm like PageRank algorithm ensures the quality Project/Idea gets proper exposure. It ranks the Project based on number of upvotes it gets considering its quality and amount of upvotes. i.e. a user with higher �User Rank� upvotes any other users project, that upvoted projects �Project Rank� will be scaled up according to Upvoters �User Rank�. This functionality helps to get optimum search results and Recommendations.
Introduction
Procrate, is an online project sharing website. Students will be able to Upload, Download and Review the projects on the website. The students as well as faculty can login in the system. Firstly, the requirements were identified which can be classified as user requirements and functional requirements.

User Requirements:
Students:
Common interdisciplinary project sharing platform at institute level.
Personal space to upload and work on project.
Customizable search option.
Comments Section for each project.
Upvote/ downvote option for projects.
Request download of project reports.
�Ask for help� forum.
Private chat box.
Pull request to provide any suggestion.
Accessibility to read and write blogs, relating to a project that is uploaded, also about any idea.
Accessibility to follow any user registered on Procrate.
Teachers:
Plagiarism checker.
Report the project.

Functional Requirements:
 Login.
 Signup.
 Add personal details.
 Create project.
 Add branch.
 Comment.
 Search.
 Upvote/Downvote.
 Check Plagiarism.
